<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Looping I Love PHP</h1>
    <?php
    echo "<h3>Looping Pertama</h3>";
    echo "<h5>Looping 1</h5>";
    for ($i=2; $i<=20; $i+=2){
        echo $i . "- I Love PHP <br>";
    }
    echo "<h3>Looping Kedua</h3>";
    echo "<h5>Looping 2</h5>";
    for ($a=20; $a>=2; $a-=2){
        echo $a . "- I Love PHP <br>";
    }

    echo "<h3>Looping Ketiga</h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo "Array number : ";
    print_r($number);
    echo"<br>";
    echo "Hasil sisa dari Array Number";
    foreach($number as $value){
        $rest[] = $value %= 5;
    }
    print_r($rest);
    echo "<br>";

    echo "<h3>Looping Asociate</h3>";
    $setkomputer =[
        ['001','Keyboard Logitek', 60000 ,'Keyboard yang mantap untuk kantoran','logitek.jpeg'],
        ['002','Keyboard MSI', 300000 ,'Keyboard gaming MSI mekanik','msi.jpeg'],
        ['003','Mouse Genius', 50000 ,'Mouse Genius biar lebih pintar','genius.jpeg'],
        ['004','Mouse Jerry', 30000,'Mouse yang disukai kucing','jerry.jpeg'],
    ];

    foreach ($setkomputer as $keys => $value){
        $item = array (
            ["id"=> "$value[0]",
             "name"=> "$value[1]",
             "harga"=> "$value[2]",
             "deskripsi"=> "$value[3]",
             "source"=>"$value[4]"]
            
            );

        print_r($item);
        echo "<br>";
    }

    echo "<h3>Looping Keempat</h3>";
    for ($j=1; $j<=5 ; $j++){
        for ($k=1; $k<=$j  ; $k++){
            echo "*";
        }
        echo "<br>";
        
    }






?>

</body>
</html>