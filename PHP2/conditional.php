<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Conditional 1</h2>

    <?php

    echo "<h3>Soal 1 Greeting</h3>";

    function greetings($nama){
        echo "Halo" . $nama . ",Selamat Datang di Sanbercode! <br>";
    }
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<br>";

    echo "<h3>Soal 2 Reverse String</h3>";

    function reverse($kata1){
        $panjangkata = strlen($kata1);
        $tampung = null;
        for($i=($panjangkata - 1); $i>=0; $i--){
            $tampung .= $kata1[$i];
        }
        return $tampung;
    }

    function reverseString($kata2){
        $fungsireverse = reverse($kata2);
        echo $fungsireverse ."<br>";
    }

    reverseString("abduh");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    

    /* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


echo"<h3>Soal 3 Palindrome</h3>";

function reverse1($kata1){
    $panjangkata = strlen($kata1);
    $tampung = null;
    for($i=($panjangkata - 1); $i>=0; $i--){
        $tampung .= $kata1[$i];
    }
    return $tampung;
}

function palindrome($cekme){
    $palin=reverse1($cekme);
    if($cekme==$palin){
        echo "$cekme:true<br>";
    }
    else{
        echo "$cekme:falese<br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
palindrome("civic") ; 
palindrome("nababan") ;
palindrome("jambaban"); 
palindrome("racecar"); 

    

    echo"<h3>Soal No 4 Tentukan Nilai</h3>";
    
    /*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini

function tentukan_nilai($angka){
    $output = null;
    if ($angka>= 85 && $angka<=100){
        return "Sangat Baik<br>";
    }else if($angka >=76 && $angka<=85){
        return "Baik<br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); 
echo tentukan_nilai(76); 
echo tentukan_nilai(67); 
echo tentukan_nilai(43); 


    
    ?>

</body>
</html>